<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230501072755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE buteurs_convocation DROP FOREIGN KEY FK_60B1516B454FFA04');
        $this->addSql('ALTER TABLE buteurs_convocation DROP FOREIGN KEY FK_60B1516BE8746F65');
        $this->addSql('ALTER TABLE buteurs_joueurs DROP FOREIGN KEY FK_CC6BC53A454FFA04');
        $this->addSql('ALTER TABLE buteurs_joueurs DROP FOREIGN KEY FK_CC6BC53AA3DC7281');
        $this->addSql('DROP TABLE buteurs');
        $this->addSql('DROP TABLE buteurs_convocation');
        $this->addSql('DROP TABLE buteurs_joueurs');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE buteurs (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE buteurs_convocation (buteurs_id INT NOT NULL, convocation_id INT NOT NULL, INDEX IDX_60B1516B454FFA04 (buteurs_id), INDEX IDX_60B1516BE8746F65 (convocation_id), PRIMARY KEY(buteurs_id, convocation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE buteurs_joueurs (buteurs_id INT NOT NULL, joueurs_id INT NOT NULL, INDEX IDX_CC6BC53A454FFA04 (buteurs_id), INDEX IDX_CC6BC53AA3DC7281 (joueurs_id), PRIMARY KEY(buteurs_id, joueurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE buteurs_convocation ADD CONSTRAINT FK_60B1516B454FFA04 FOREIGN KEY (buteurs_id) REFERENCES buteurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE buteurs_convocation ADD CONSTRAINT FK_60B1516BE8746F65 FOREIGN KEY (convocation_id) REFERENCES convocation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE buteurs_joueurs ADD CONSTRAINT FK_CC6BC53A454FFA04 FOREIGN KEY (buteurs_id) REFERENCES buteurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE buteurs_joueurs ADD CONSTRAINT FK_CC6BC53AA3DC7281 FOREIGN KEY (joueurs_id) REFERENCES joueurs (id) ON DELETE CASCADE');
    }
}
