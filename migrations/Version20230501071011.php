<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230501071011 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE convocation_joueurs DROP FOREIGN KEY FK_D1304284E8746F65');
        $this->addSql('ALTER TABLE convocation_joueurs DROP FOREIGN KEY FK_D1304284A3DC7281');
        $this->addSql('DROP TABLE convocation_joueurs');
        $this->addSql('ALTER TABLE joueurs DROP buts');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE convocation_joueurs (convocation_id INT NOT NULL, joueurs_id INT NOT NULL, INDEX IDX_D1304284A3DC7281 (joueurs_id), INDEX IDX_D1304284E8746F65 (convocation_id), PRIMARY KEY(convocation_id, joueurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE convocation_joueurs ADD CONSTRAINT FK_D1304284E8746F65 FOREIGN KEY (convocation_id) REFERENCES convocation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE convocation_joueurs ADD CONSTRAINT FK_D1304284A3DC7281 FOREIGN KEY (joueurs_id) REFERENCES joueurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE joueurs ADD buts SMALLINT DEFAULT NULL');
    }
}
