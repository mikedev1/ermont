<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230429140536 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueurs ADD poste_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE joueurs ADD CONSTRAINT FK_F0FD889DA0905086 FOREIGN KEY (poste_id) REFERENCES poste (id)');
        $this->addSql('CREATE INDEX IDX_F0FD889DA0905086 ON joueurs (poste_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueurs DROP FOREIGN KEY FK_F0FD889DA0905086');
        $this->addSql('DROP INDEX IDX_F0FD889DA0905086 ON joueurs');
        $this->addSql('ALTER TABLE joueurs DROP poste_id');
    }
}
