<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230429131111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE joueurs_poste (joueurs_id INT NOT NULL, poste_id INT NOT NULL, INDEX IDX_1717A053A3DC7281 (joueurs_id), INDEX IDX_1717A053A0905086 (poste_id), PRIMARY KEY(joueurs_id, poste_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE joueurs_poste ADD CONSTRAINT FK_1717A053A3DC7281 FOREIGN KEY (joueurs_id) REFERENCES joueurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE joueurs_poste ADD CONSTRAINT FK_1717A053A0905086 FOREIGN KEY (poste_id) REFERENCES poste (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueurs_poste DROP FOREIGN KEY FK_1717A053A3DC7281');
        $this->addSql('ALTER TABLE joueurs_poste DROP FOREIGN KEY FK_1717A053A0905086');
        $this->addSql('DROP TABLE joueurs_poste');
    }
}
