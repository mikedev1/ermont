<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230430103727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adversaire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE convocation ADD adversaire_id INT DEFAULT NULL, ADD resultat VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE convocation ADD CONSTRAINT FK_C03B3F5F3E4689F5 FOREIGN KEY (adversaire_id) REFERENCES adversaire (id)');
        $this->addSql('CREATE INDEX IDX_C03B3F5F3E4689F5 ON convocation (adversaire_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE convocation DROP FOREIGN KEY FK_C03B3F5F3E4689F5');
        $this->addSql('DROP TABLE adversaire');
        $this->addSql('DROP INDEX IDX_C03B3F5F3E4689F5 ON convocation');
        $this->addSql('ALTER TABLE convocation DROP adversaire_id, DROP resultat');
    }
}
