<?php

namespace App\Components;

use App\Repository\ConvocationRepository;
use App\Repository\JoueursRepository;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;

#[AsLiveComponent('search_convocation')]
class ConvocationSearch
{
    use DefaultActionTrait;

    #[LiveProp('writable: true')]
    public string $query = '';

    public function __construct(
        private ConvocationRepository $convocationRepository,
        private JoueursRepository $joueursRepository
    ) {
    }

    public function getConvocations(): array
    {
        return $this->convocationRepository->findByQuery($this->query);
    }
    public function getJoueurs(): array
    {
        return $this->joueursRepository->findByQuery($this->query);
    }
}
