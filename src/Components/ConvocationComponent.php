<?php

namespace App\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('convocation')]
class ConvocationComponent
{
    public $convocation;
    
}
