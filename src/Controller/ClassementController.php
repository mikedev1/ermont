<?php

namespace App\Controller;

use App\Repository\JoueursRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClassementController extends AbstractController
{

    #[Route('/classement', name: 'app_classement')]
    public function index(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request, JoueursRepository $joueursRepository): Response
    {

        $dql   = "SELECT j FROM App\Entity\Joueurs j";
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('classement/index.html.twig', [
            'controller_name' => 'ClassementController',
            'pagination' => $pagination,
            'joueurs' => $query,
        ]);
    }
}
