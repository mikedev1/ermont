<?php

namespace App\Controller;

use App\Repository\JoueursRepository;
use App\Repository\PosteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EffectifController extends AbstractController
{

    private JoueursRepository $joueursRepository;
    private PosteRepository $posteRepository;

    public function __construct(
        JoueursRepository $joueursRepository,
        PosteRepository $posteRepository
    ) {
        $this->joueursRepository = $joueursRepository;
        $this->posteRepository = $posteRepository;
    }

    #[Route('/effectif', name: 'app_effectif')]
    public function index(): Response
    {
        $joueurs = $this->joueursRepository->findAll();
        $postes = $this->posteRepository->findAll();

        return $this->render('effectif/index.html.twig', [
            'joueurs' => $joueurs,
            'postes' => $postes,
        ]);
    }
}
