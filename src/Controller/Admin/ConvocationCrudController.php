<?php

namespace App\Controller\Admin;

use App\Entity\Joueurs;
use App\Form\JoueursType;
use App\Entity\Convocation;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\TextEditorType;

class ConvocationCrudController extends AbstractCrudController
{
    //public JoueursType $joueursType;
/* 
    public function __construct(JoueursType $joueursType){
        $this->joueursType = $joueursType;
    } */

    public static function getEntityFqcn(): string
    {
        return Convocation::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt'),
            AssociationField::new('adversaire'),
            AssociationField::new('joueurs'),
            TextEditorField::new('resultat'),
            TextEditorField::new('description'),
        ];
    }
}
