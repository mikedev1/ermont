<?php

namespace App\Controller\Admin;

use App\Entity\Joueurs;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class JoueursCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Joueurs::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('prenom'),
            TextField::new('passes'),
            AssociationField::new('convocations')

        ];
    }
   
}
