<?php

namespace App\Controller;

use App\Repository\ConvocationRepository;
use App\Repository\JoueursRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConvocationController extends AbstractController
{
    public ConvocationRepository $convocationRepository;

    #[Route('/', name: 'app_convocation')]
    public function index(
        ConvocationRepository $convocationRepository,
        JoueursRepository $joueursRepository
    ): Response {

        $convocations = $convocationRepository->findBy([], [
            'createdAt' => 'DESC'
        ]);
        /* Joueurs convoqués */
        $convoques = $convocationRepository->joueurConvoques();

        return $this->render('convocation/index.html.twig', [
            'convocations' => $convocations,
            'convoques' => $convoques
        ]);
    }
}
