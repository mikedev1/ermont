<?php

namespace App\Repository;

use App\Entity\Joueurs;
use App\Entity\Poste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Joueurs>
 *
 * @method Joueurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Joueurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Joueurs[]    findAll()
 * @method Joueurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoueursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Joueurs::class);
    }

    public function save(Joueurs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Joueurs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByQuery(string $query): array
    {
        if (empty($query)) {
            return [];
        }
        return $this->createQueryBuilder('c')
            ->andWhere('c.nom LIKE :query OR c.prenom LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->orderBy('c.prenom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findPerPagination()
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->orderBy('j.prenom')
            ->getQuery()
            ->getResult();
    }



    //    /**
    //     * @return Joueurs[] Returns an array of Joueurs objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('j')
    //            ->andWhere('j.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('j.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Joueurs
    //    {
    //        return $this->createQueryBuilder('j')
    //            ->andWhere('j.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
