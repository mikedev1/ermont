<?php

namespace App\Repository;

use App\Entity\Adversaire;
use App\Entity\Joueurs;
use App\Entity\Convocation;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Convocation>
 *
 * @method Convocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Convocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Convocation[]    findAll()
 * @method Convocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConvocationRepository extends ServiceEntityRepository
{
    public JoueursRepository $joueursRepository;
    public int $id;
    public function __construct(
        ManagerRegistry $registry,
        JoueursRepository $joueursRepository
    ) {
        parent::__construct($registry, Convocation::class);
        $this->joueursRepository = $joueursRepository;
    }

    public function save(Convocation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Convocation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function joueurConvoques()
    {
        return $this->createQueryBuilder('c')
            ->select('j')
            ->from(Joueurs::class, 'j')
            ->join('j.convocations', 'jc')
            ->join('c.joueurs', 'cj')
            ->where('c.id = jc.id')
            ->getQuery()
            ->getResult();
    }


    public function findByQuery(string $query): array
    {
        if (empty($query)) {
            return [];
        }
        return $this->createQueryBuilder('c')
            ->from(Adversaire::class, 'a')
            ->join('c.adversaire', 'ca')
            ->andWhere('a.nom LIKE :query')
            ->andWhere('a.id = c.adversaire')
            ->setParameter('query', '%' . $query . '%')
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
    //    /**
    //     * @return Convocation[] Returns an array of Convocation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Convocation
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
