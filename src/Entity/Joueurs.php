<?php

namespace App\Entity;

use App\Repository\JoueursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JoueursRepository::class)]
class Joueurs
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column(length: 50)]
    private ?string $prenom = null;

    #[ORM\ManyToOne(inversedBy: 'joueurs')]
    private ?Poste $poste = null;

    #[ORM\ManyToMany(targetEntity: Convocation::class, mappedBy: 'joueurs')]
    private Collection $convocations;



    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $passes = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $buts = null;

 

    public function __construct()
    {
        $this->convocations = new ArrayCollection();
    }
   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getPoste(): ?Poste
    {
        return $this->poste;
    }

    public function setPoste(?Poste $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    /**
     * @return Collection<int, Convocation>
     */
    public function getConvocations(): Collection
    {
        return $this->convocations;
    }

    public function addConvocation(Convocation $convocation): self
    {
        if (!$this->convocations->contains($convocation)) {
            $this->convocations->add($convocation);
            $convocation->addJoueur($this);
        }

        return $this;
    }

    public function removeConvocation(Convocation $convocation): self
    {
        if ($this->convocations->removeElement($convocation)) {
            $convocation->removeJoueur($this);
        }

        return $this;
    }


    public function __toString(){
        return $this->getPrenom();
    }



    public function getPasses(): ?int
    {
        return $this->passes;
    }

    public function setPasses(?int $passes): self
    {
        $this->passes = $passes;

        return $this;
    }

    public function getButs(): ?int
    {
        return $this->buts;
    }

    public function setButs(?int $buts): self
    {
        $this->buts = $buts;

        return $this;
    }




}
