<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {

    
         $user = new User();
         $user  ->setEmail("servam95@gmail.com")
                ->setPassword("aaaaaa")
                ->setRoles(["ROLE_ADMIN"]);

        $manager->persist($user);
dump($user);
        $manager->flush(); 
    }
}
