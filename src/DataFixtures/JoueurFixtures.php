<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Poste;
use App\Entity\Couleur;
use App\Entity\Joueurs;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\HttpClient\Chunk\FirstChunk;

class JoueurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');


        $postes = ["Goal", "Déf", "Mil", "Atq"];
        $couleurs = ["danger", "dark", "info", "success"];



        for ($i = 0; $i < count($postes); $i++) {
            /* Couleurs */
            $couleur = new Couleur();
            $couleur->setNom($couleurs[$i]);

            $manager->persist($couleur);
            /* Poste */
            $poste = new Poste();
            $poste->setNom($postes[$i]);
            $poste->setCouleur($couleur);

            $manager->persist($poste);
            /* Gardien */
            if ($poste->getNom() == $postes[0]) {
                $joueur = new Joueurs();
                $joueur->setPrenom($faker->FirstNameMale());
                $joueur->setNom($faker->lastName());
                $joueur->setPoste($poste);
                $manager->persist($joueur);
            }
            /* Defenseur */
            if ($poste->getNom() == $postes[1]) {
                for ($x = 0; $x < 7; $x++) {
                    $joueur = new Joueurs();
                    $joueur->setPrenom($faker->FirstNameMale());
                    $joueur->setNom($faker->lastName());
                    $joueur->setPoste($poste);
                    $manager->persist($joueur);
                }
            }
            /* Milieu */
            if ($poste->getNom() == $postes[2]) {
                for ($y = 0; $y < 8; $y++) {
                    $joueur = new Joueurs();
                    $joueur->setPrenom($faker->FirstNameMale());
                    $joueur->setNom($faker->lastName());
                    $joueur->setPoste($poste);
                    $manager->persist($joueur);
                }
            }
               /* Attaquant */
               if ($poste->getNom() == $postes[3]) {
                for ($z = 0; $z < 4; $z++) {
                    $joueur = new Joueurs();
                    $joueur->setPrenom($faker->FirstNameMale());
                    $joueur->setNom($faker->lastName());
                    $joueur->setPoste($poste);
                    $manager->persist($joueur);
                }
            }
            
            $manager->persist($joueur);
        }

        $manager->flush();
    }
}
