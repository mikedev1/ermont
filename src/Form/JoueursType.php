<?php

namespace App\Form;

use App\Entity\Joueurs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class JoueursType extends AbstractType
{
    public Joueurs $joueurs;
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            -> add('nom', EntityType::class, [
                'class' => Joueurs::class,
                'label' => false,
                'choice_value' => 'nom',
                'placeholder' => "Rechercher",
                'choice_label' => "nom",
                'multiple' => false,
            ])
            ->add('poste')
            ->add('buts');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Joueurs::class,
        ]);
    }
}
